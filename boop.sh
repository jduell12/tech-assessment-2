#!/usr/bin/env bash
set -e

COMMENT=$(psql -h postgres -U tech_user -c "SELECT pg_catalog.obj_description(c.oid) FROM pg_namespace c WHERE c.nspname = 'tech_user';" tech)
echo $COMMENT
INITIALIZED=""
INITIALIZED=$(echo $COMMENT | grep initialized || true)
echo "schema status=$INITIALIZED"
if [ -z "$INITIALIZED" ]; then
  echo "importing db.."
  psql -h postgres -U tech_user -f "/db-init.sql" tech
fi