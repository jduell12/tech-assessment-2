from setuptools import find_packages, setup

DEV_REQUIRES = [
    "flask",
    "flask-cors",
    "mypy",
    "pytest",
    "pytest-cov",
]

setup(
    name="backend",
    version="0.0.1",
    description="backend",
    python_requires=">=3.7",
    packages=find_packages("."),
    entry_points={
        "console_scripts": {
            "local-api = backend.api:main",
        }
    },
    install_requires=[
        "more-itertools",
        "psycopg2-binary",
        "sqlalchemy",
        "typing-extensions",
    ],
    extras_require={"dev": DEV_REQUIRES},
    include_package_data=True,
)
