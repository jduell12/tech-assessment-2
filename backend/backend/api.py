from flask import Flask, json
from flask_cors import CORS


api = Flask(__name__)
CORS(api)

@api.route('/route', methods=['GET'])
def get_route():
    return json.dumps([{}])


def main():
    api.run()


if __name__ == '__main__':
    main()
